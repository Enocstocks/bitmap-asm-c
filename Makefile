AS = as
CC = gcc
TESTFLAGS = `pkg-config --cflags --libs cmocka`
asmfiles = lib/bitmap.asm
cfiles = lib/bitmap.c
objects = bitmap-c.o bitmap-asm.o

.PHONY: tests run-tests all clean

reqs = bitmap.a $(objects)

all: $(reqs)

bitmap-asm.o: $(asmfiles)
	$(AS) $^ -o $@

bitmap-c.o: $(cfiles)
	$(CC) -g -O -c $^ -o $@

bitmap.a: $(objects)
	ar mc $@ $^

tests/tests: tests/bitmap-asm.c bitmap.a
	$(CC) $^ -o $@ $(TESTFLAGS)

check: tests/tests
	./tests/tests

clean:
	-rm bitmap.a
	-rm bitmap-c.o
	-rm bitmap-asm.o
	-rm tests/tests
