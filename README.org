* Overview
bitmap-asm-c is a tiny library to handle bitmaps, not only bitmaps but
also individual bits for all types, made in Assembly and C. The
assembly code is for x86 architectures but it should work in any
architecture with "~bt~" and "~bts~" instructions.

* Why assembly?
It happens that the C standard library does not have functions to
manipulate individual bits, so I tried to use bitwise shifting
operations but those operations are pretty limited to use with
individual bits.

Bitwise operations can handle bits, in a very limited way though.
Shifting n bits a number can "manipulate" bits, e. g:

#+BEGIN_SRC c
// Exactly 8 bits bitmap regardless architecture
uint8_t bitmap = 0;

// Setting 0th bit with left shift operation
bitmap = bitmap | (1 << 0); // now bitmap has value 1
#+END_SRC

The previous code works, but it will only work with complete data
types like ~int~ and ~int *~, but it will not work with ~void *~ (due
to its semantics it cannot be assigned to a value via dereference),
which is the best type to make a runtime size bitmap.

Another problem is raised when bitmaps are made with integer types,
the bitmap will be limited to the bits of the type; and, pointers to
integer types will be limited to the lvalue promotion, e, g.

#+BEGIN_SRC c
// 8 bits bitmap
uint8_t *bitmap = malloc(sizeof (uint8_t));

// setting all bits
bitmap = 0b11111111;

*bitmap = *bitmap | (1 << 8);
// the value of the previous line is still 0b11111111,
// the value is the same and the bit of the adyacent
// memory addres value is not set, which means it is
// stuck to 8 bits. Since it is a heap allacated value
// it can be realloc but it is still stuck to intmax_t
// bits as much
#+END_SRC

The previous block of code behavior is defined by the C standard
(9899:2017) in "Bitwise shift operators" section, and it says:

#+BEGIN_QUOTE
The integer promotions are performed on each of the operands. The type of the result is that of the
promoted left operand. If the value of the right operand is negative or is greater than or equal to the
width of the promoted left operand, the behavior is undefined.
#+END_QUOTE

* How to use
 First clone this repo and run ~make~ or ~make -e TEST=off~ (compile
without running test).

Now a ~bitmap.a~ static should appear in the current directory, and
you are ready to compile the library with your programs.

* Dependencies
- cmocka (only needed for the tests).
- Gas (GNU assembler), this assembler is needed to compile
  bitmap-asm.asm file due the use of assembly AT&T syntax.
