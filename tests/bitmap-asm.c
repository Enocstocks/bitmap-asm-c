#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdint.h>
#include <string.h>
#include "../lib/bitmap.h"

static void test_set_bit(void ** state) {
  void * bitmap = malloc(sizeof(int));

  set_bit(bitmap, 4);
  assert_int_equal(* (int *) bitmap, 16);
  free(bitmap);
}

static void test_test_bit_1(void ** state) {
  int bitmap = 0b00000100;

  int ans = test_bit(&bitmap, 2);
  assert_int_equal(ans, 1);
}

static void test_test_bit_0(void ** state) {
  unsigned int bitmap = 0b11011111;

  int s = test_bit(&bitmap, 5);
  assert_int_equal(s, 0);
}

static void test_unset_bit(void ** state) {
  int bitmap = 0b00010100;

  unset_bit(&bitmap, 4);
  assert_int_equal(bitmap, 4);
}

static void test_set_317nth_bit(void ** state) {
  void * bitmap = calloc(5, sizeof(int64_t));

  set_bit(bitmap, 317);
  int s = test_bit(bitmap, 317);
  assert_int_equal(s, 1);
  free(bitmap);
}

static void test_find_unset_bit(void ** state) {
  int bitmap = 0b0000100011111111;

  int s = find_unset_bit(&bitmap, 0, 31);
  assert_int_equal(s, 8);
}

static void test_find_unset_bit_offset(void ** state) {
  int bitmap = 0b0010100000000000;

  int s = find_unset_bit(&bitmap, 11, 31);
  assert_int_equal(s, 12);
}

static void test_find_set_bit(void ** state) {
  void * bitmap = calloc(5, sizeof(int64_t));

  set_bit(bitmap, 255);
  int s = find_set_bit(bitmap, 0, 320);
  assert_int_equal(s, 255);
  free(bitmap);
}

static void test_find_set_bit_offset(void ** state) {
  void * bitmap = calloc(5, sizeof(int64_t));

  set_bit(bitmap, 30);
  set_bit(bitmap, 45);
  int s = find_set_bit(bitmap, 31, 320);
  assert_int_equal(s, 45);
  free(bitmap);
}

static void test_macro_bits_of(void ** state) {
  assert_int_equal(bits_of(uint16_t), 16);
}

static void test_macro_cbits_of(void ** state) {
  assert_int_equal(cbits_of(uint64_t, 6), 384);
}

int main(void) {
  const struct CMUnitTest test[10] = {
    cmocka_unit_test(test_set_bit),
    cmocka_unit_test(test_test_bit_1),
    cmocka_unit_test(test_unset_bit),
    cmocka_unit_test(test_set_317nth_bit),
    cmocka_unit_test(test_test_bit_0),
    cmocka_unit_test(test_find_unset_bit),
    cmocka_unit_test(test_find_set_bit),
    cmocka_unit_test(test_find_set_bit_offset),
    cmocka_unit_test(test_macro_bits_of),
    cmocka_unit_test(test_macro_cbits_of)
  };

  cmocka_run_group_tests(test, NULL, NULL);
}
