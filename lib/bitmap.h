#ifndef _BITMAP_ASM_C_
#include <limits.h>

#define _BITMAP_ASM_C_

#define bits_of(a) sizeof(a) * CHAR_BIT

#define cbits_of(a, b) bits_of(a) * b

void set_bit(void * bitmap, int pos);

void unset_bit(void * bitmap, int pos);

int test_bit(void *bitmap, int pos);

int find_unset_bit(void * bitmap, int offset, int limit);

int find_set_bit(void * bitmap, int offset, int limit);

#endif
