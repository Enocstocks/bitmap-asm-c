#include "bitmap.h"

int find_unset_bit(void * bitmap, int offset, int limit) {
  for (int i = offset; i < limit; i++)
    if (test_bit(bitmap, i) == 0)
      return i;

  return -1;
}

int find_set_bit(void * bitmap, int offset, int limit) {
  for (int i = offset; i < limit; i++)
    if (test_bit(bitmap, i) == 1)
      return i;

  return -1;
}
