.section .data

.section .bss

.section .text
        .globl set_bit
        .globl test_bit
        .globl unset_bit
        .type set_bit, @function
        .type test_bit, @function
        .type unset_bit, @function

# Set bit in position %rsi of the value pointed by %rdi
set_bit:
        push %rbp
        mov %rsp, %rbp

        bts %rsi, (%rdi)

        leave
        ret

# Unset bit in position %rsi of the value pointed by %rdi
unset_bit:
        push %rbp
        mov %rsp, %rbp

        btr %rsi, (%rdi)

        leave
        ret

# Return 1 if bit in position %rsi is set, return 0 otherwise
test_bit:
        push %rbp
        mov %rsp, %rbp

        bt %rsi, (%rdi)
        jb 1f
        mov $0, %rax
        jmp 0f

1:
        mov $1, %rax

0:
        leave
        ret
